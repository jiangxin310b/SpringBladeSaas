﻿using System;
using SpringBlade.Common;
using SpringBlade.Core.Model.Sys;
using SpringBlade.Service.DtoModel;
using System.Threading.Tasks;

namespace SpringBlade.Service.Interfaces
{
    /// <summary>
    /// 系统日志业务接口
    /// </summary>
    public interface ISysLogService : IBaseServer<SysLog>
    {
        /// <summary>
        /// 获得列表
        /// </summary>
        /// <returns></returns>
        new Task<ApiResult<Page<SysLog>>> GetPagesAsync(PageParm parm);
        
    }
}
