﻿using SpringBlade.Common;
using SpringBlade.Core.Model.Sys;
using SpringBlade.Service.DtoModel;
using System.Threading.Tasks;

namespace SpringBlade.Service.Interfaces
{
    public interface ISysAppSettingService : IBaseServer<SysAppSetting>
    {
      
    }
}
