﻿
using SpringBlade.WeiXinCore.Model.Wx;
using SpringBlade.Extensions;
using SqlSugar;

namespace SpringBlade.WeiXinCore
{
    /// <summary>
    /// 数据库上下文
    /// </summary>
    public class DbContext
    {
        public DbContext()
        {
            Db = new SqlSugarClient(new ConnectionConfig()
            {
                ConnectionString = ConfigExtensions.Configuration["DbConnection:WeiXinMySqlConnectionString"],
                DbType = DbType.MySql,
                IsAutoCloseConnection = true
            });
            //调式代码 用来打印SQL 
            Db.Aop.OnLogExecuting = (sql, pars) =>
            {
                string s = sql;
                //Console.WriteLine(sql + "\r\n" +
                //    Db.Utilities.SerializeObject(pars.ToDictionary(it => it.ParameterName, it => it.Value)));
                //Console.WriteLine();
            };
        }
        public SqlSugarClient Db;//用来处理事务多表查询和复杂的操作
        public DbSet<DbModel> GetDb<DbModel>() where DbModel : class, new()
        {
            return new DbSet<DbModel>(Db);
        }

        //微信设置
        public DbSet<WxSetting> WxSettingDb => new DbSet<WxSetting>(Db);
        public DbSet<WxMaterial> WxMaterialDb => new DbSet<WxMaterial>(Db);

     
    }
    /// <summary>
    /// 扩展ORM
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class DbSet<T> : SimpleClient<T> where T : class, new()
    {
        public DbSet(SqlSugarClient context) : base(context)
        {

        }
        /// <summary>
        /// 扩展假删除功能
        /// </summary>
        /// <typeparam name="DbModel"></typeparam>
        /// <param name="dbModel"></param>
        /// <returns></returns>
        public bool FalseDelete<DbModel>(DbModel dbModel) where DbModel : BaseDbModel, new()
        {
            return this.Context.Updateable<DbModel>(dbModel).UpdateColumns(it => new DbModel() { IsDel = true }).ExecuteCommand() > 0;
        }
        /// <summary>
        /// 扩展假删除功能
        /// </summary>
        /// <typeparam name="DbModel"></typeparam>
        /// <param name="dbModel"></param>
        /// <returns></returns>
        public bool FalseDelete<DbModel>(DbModel[] dbModels) where DbModel : BaseDbModel, new()
        {
            return this.Context.Updateable<DbModel>(dbModels).UpdateColumns(it => new DbModel() { IsDel = true }).ExecuteCommand() > 0;
        }
    }
}
