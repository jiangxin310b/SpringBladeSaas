/*
Navicat MySQL Data Transfer

Source Server         : fyt
Source Server Version : 50719
Source Host           : localhost:3306
Source Database       : fyt_cms

Target Server Type    : MYSQL
Target Server Version : 50719
File Encoding         : 65001

Date: 2019-03-17 18:45:59
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for wx_material
-- ----------------------------
DROP TABLE IF EXISTS `wx_material`;
CREATE TABLE `wx_material` (
  `Id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自动增长',
  `WxId` int(11) NOT NULL COMMENT '所属公众号ID',
  `Type` tinyint(4) NOT NULL DEFAULT '1' COMMENT '类型，1=图文  2=连接',
  `Position` tinyint(4) NOT NULL DEFAULT '0' COMMENT '保存位置  1=本地 2=服务器',
  `Title` varchar(50) NOT NULL COMMENT '标题',
  `Img` varchar(255) DEFAULT NULL COMMENT '图片',
  `Summary` varchar(500) DEFAULT NULL COMMENT '描述',
  `Author` varchar(20) DEFAULT NULL COMMENT '作者',
  `Link` varchar(255) DEFAULT NULL COMMENT '连接',
  `Content` text COMMENT '内容',
  `TestJson` text COMMENT '内容Json',
  `AddDate` datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '添加时间',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wx_material
-- ----------------------------
INSERT INTO `wx_material` VALUES ('6', '2', '2', '1', '这里面测试的是连接图文', '/upload/localpc/banner/583bf138e46ad_1024.jpg', 'ddddddddddddddddddddddddd', '', 'sssssssssss', '', '[{\"id\":6,\"wxId\":2,\"type\":2,\"position\":1,\"title\":\"这里面测试的是连接图文\",\"img\":\"/upload/localpc/banner/583bf138e46ad_1024.jpg\",\"summary\":\"ddddddddddddddddddddddddd\",\"author\":\"\",\"link\":\"sssssssssss\",\"content\":\"\",\"testJson\":\"[{\\\"title\\\":\\\"这里面测试的是连接图文\\\",\\\"img\\\":\\\"/upload/localpc/banner/583bf138e46ad_1024.jpg\\\",\\\"author\\\":\\\"\\\",\\\"link\\\":\\\"sssssssssss\\\",\\\"content\\\":\\\"\\\",\\\"summary\\\":\\\"\\\"}]\",\"addDate\":\"2019-03-03T19:31:00\"}]', '2019-03-03 19:43:43');
INSERT INTO `wx_material` VALUES ('7', '2', '1', '1', '正经的测试标题', 'http://img.feiyit.com/feiyit/website/else/cc.jpg', 'sssdsdsdssddddddddddd', '作者', 'http://www.feiyit.com', '<p>这里面是内容，内容内容</p>\n<p>回车内容</p>', '[{\"id\":7,\"wxId\":2,\"type\":1,\"position\":1,\"title\":\"正经的测试标题\",\"img\":\"http://img.feiyit.com/feiyit/website/else/cc.jpg\",\"summary\":\"sssdsdsdssddddddddddd\",\"author\":\"作者\",\"link\":\"http://www.feiyit.com\",\"content\":\"<p>这里面是内容，内容内容</p>\\n<p>回车内容</p>\",\"testJson\":\"[{\\\"title\\\":\\\"正经的测试标题\\\",\\\"img\\\":\\\"http://img.feiyit.com/feiyit/website/else/cc.jpg\\\",\\\"author\\\":\\\"作者\\\",\\\"link\\\":\\\"http://www.feiyit.com\\\",\\\"content\\\":\\\"<p>这里面是内容，内容内容</p>\\\\n<p>回车内容</p>\\\",\\\"summary\\\":\\\"\\\"}]\",\"addDate\":\"2019-03-03T19:39:15\"}]', '2019-03-03 19:44:13');

-- ----------------------------
-- Table structure for wx_setting
-- ----------------------------
DROP TABLE IF EXISTS `wx_setting`;
CREATE TABLE `wx_setting` (
  `Id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自动增长',
  `Name` varchar(50) NOT NULL COMMENT '公众号名称',
  `Account` varchar(50) NOT NULL COMMENT '公众平台微信号',
  `OriginalId` varchar(50) NOT NULL COMMENT '公众号原始ID',
  `AppId` varchar(50) NOT NULL COMMENT 'ToKen',
  `AppSecret` varchar(255) NOT NULL COMMENT 'AppSecret',
  `Type` varchar(20) NOT NULL COMMENT '公众号类型',
  `Cover` varchar(255) DEFAULT NULL COMMENT '公众号图片',
  `QrCode` varchar(255) DEFAULT NULL COMMENT '公众号二维码',
  `Status` bit(1) NOT NULL DEFAULT b'1' COMMENT '状态',
  `MenuJson` varchar(2000) DEFAULT NULL COMMENT '自定义菜单Json',
  `AddDate` datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '添加时间',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wx_setting
-- ----------------------------
INSERT INTO `wx_setting` VALUES ('2', '测试公众号', 'wx554654654', 'g_sddwsssss', 'wx77922334429502', '9bfed87a0157250c26479a084c207b12', '认证服务号/认证媒体/政府订阅号', null, null, '', '[{\"name\":\"A级菜单\",\"url\":\"http://h5.feiyit.com/\",\"type\":0,\"sub_button\":[]},{\"name\":\"B级菜单\",\"url\":\"http://h5.feiyit.com/\",\"type\":0,\"sub_button\":[]},{\"name\":\"C级菜单\",\"url\":\"\",\"type\":\"0\",\"sub_button\":[{\"name\":\"搜索\",\"type\":0,\"url\":\"http://www.soso.com/\"},{\"name\":\"视频\",\"type\":0,\"url\":\"http://v.qq.com/\"},{\"name\":\"赞一下我们\",\"type\":0,\"url\":\"http://www.feiyit.com/\"}]}]', '2019-03-17 18:45:44');
