﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SpringBlade.Common;
using SpringBlade.Core.Model.Sys;
using SpringBlade.Service.DtoModel;
using SpringBlade.Service.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace SpringBlade.Api.Controllers
{
    [Produces("application/json")]
    [Route("api/admin")]
    public class SysAdminController : Controller
    {
        private readonly ISysAdminService _sysAdminService;
        public SysAdminController(ISysAdminService sysAdminService)
        {
            _sysAdminService = sysAdminService;
        }

        /// <summary>
        /// 查询列表
        /// </summary>
        /// <param name="parm"></param>
        /// <returns></returns>
        [HttpGet("login")]
        public async Task<ApiResult<SysAdmin>> LoginAsync(SysAdminLogin parm)
        {
            return await _sysAdminService.LoginAsync(parm);
        }
    }
}