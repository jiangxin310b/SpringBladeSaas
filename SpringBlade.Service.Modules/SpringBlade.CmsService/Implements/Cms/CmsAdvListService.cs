﻿using SpringBlade.Common;
using SpringBlade.CmsCore.Model.Cms;
using System.Collections.Generic;
using System.Threading.Tasks;
using SpringBladeIms.CmsService.Extensions;
using SpringBlade.CmsCore;
using SpringBlade.CmsService.DtoModel;
using SpringBlade.CmsService.Interfaces;
using SqlSugar;
using System;
using System.Text;
namespace SpringBlade.CmsService.Implements
{
    /*!
    * 文件名称：CmsAdvListService服务接口实现
    * 版权所有：北京金证科技有限公司
    * 企业官网：http://www.jinzheng.com
    */
    public class CmsAdvListService : BaseServer<CmsAdvList>, ICmsAdvListService
    {
	}
}