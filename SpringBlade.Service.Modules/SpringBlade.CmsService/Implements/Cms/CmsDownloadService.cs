﻿using SpringBladeIms.CmsService.Extensions;
using SpringBlade.Common;
using SpringBlade.CmsCore.Model.Cms;
using SpringBlade.CmsService.DtoModel;
using SpringBlade.CmsService.Interfaces;
namespace SpringBlade.CmsService.Implements
{
    /*!
    * 文件名称：CmsDownload服务接口实现
    * 版权所有：北京金证科技有限公司
    * 企业官网：http://www.jinzheng.com
    */
	public class CmsDownloadService : BaseServer<CmsDownload>, ICmsDownloadService
	{
        /// <summary>
        /// 获得列表
        /// </summary>
        /// <param name="parm"></param>
        /// <returns></returns>
        public Page<CmsDownload> GetList(PageParm parm)
        {
            return Db.Queryable<CmsDownload>()
                .WhereIF(parm.id != 0, m => m.ColumnId == parm.id)
                .WhereIF(!string.IsNullOrEmpty(parm.key), m => m.Title.Contains(parm.key) || m.Tag.Contains(parm.key) || m.Summary.Contains(parm.key))
                .WhereIF(parm.audit == 0, m => m.Audit)
                .WhereIF(parm.audit == 1, m => !m.Audit)
                .WhereIF(!string.IsNullOrEmpty(parm.where), parm.where)
                .OrderBy(m => m.Sort, SqlSugar.OrderByType.Desc)
                .OrderBy(m => m.EditDate, SqlSugar.OrderByType.Desc)
                .ToPage(parm.page, parm.limit);
        }

    }
}