﻿using SpringBlade.CmsCore.Model.Cms;
using SpringBlade.CmsService.Interfaces;
namespace SpringBlade.CmsService.Implements
{
    /*!
    * 文件名称：CmsAdvclass服务接口实现
    * 版权所有：北京金证科技有限公司
    * 企业官网：http://www.jinzheng.com
    */
	public class CmsAdvClassService : BaseServer<CmsAdvClass>, ICmsAdvClassService
	{
	}
}