﻿using SpringBlade.Common;
using SpringBlade.CmsCore.Model.Cms;
using SpringBlade.Extensions;
using SpringBlade.CmsService.DtoModel;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SpringBlade.CmsService.Interfaces
{
    /// <summary>
    /// 图片管理业务接口
    /// </summary>
    public interface ICmsImageService : IBaseServer<CmsImage>
    {
        CloudFile GetList(PageParm parm);
    }
}
