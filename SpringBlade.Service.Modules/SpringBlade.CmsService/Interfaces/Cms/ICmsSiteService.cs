﻿using SpringBlade.CmsCore.Model.Cms;

namespace SpringBlade.CmsService.Interfaces
{
    public interface ICmsSiteService : IBaseServer<CmsSite>
    {

    }
}
