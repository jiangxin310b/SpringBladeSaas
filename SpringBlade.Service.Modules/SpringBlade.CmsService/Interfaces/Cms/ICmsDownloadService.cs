﻿using SpringBlade.Common;
using SpringBlade.CmsCore.Model.Cms;
using SpringBlade.CmsService.DtoModel;

namespace SpringBlade.CmsService.Interfaces
{
    /*!
    * 文件名称：CmsDownload服务接口
    * 版权所有：北京金证科技有限公司
    * 企业官网：http://www.jinzheng.com
    */
	public interface ICmsDownloadService: IBaseServer<CmsDownload>
	{
        /// <summary>
        /// 添加一条数据
        /// </summary>
        /// <param name="parm">CmsColumn</param>
        /// <returns></returns>
        Page<CmsDownload> GetList(PageParm parm);
    }
}