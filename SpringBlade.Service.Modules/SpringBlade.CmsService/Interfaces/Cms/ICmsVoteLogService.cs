﻿using SpringBlade.CmsCore.Model.Cms;
namespace SpringBlade.CmsService.Interfaces
{
    /*!
    * 文件名称：CmsVotelog服务接口
    * 版权所有：北京金证科技有限公司
    * 企业官网：http://www.jinzheng.com
    */
	public interface ICmsVoteLogService: IBaseServer<CmsVoteLog>
	{
	}
}