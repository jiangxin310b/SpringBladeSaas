﻿using SpringBlade.MerchantCore.Model.Merchant;
namespace SpringBlade.MerchantService.Interfaces
{
    /*!
    * 文件名称：Wx_setting服务接口
    */
	public interface IWxSettingService: IBaseServer<WxSetting>
	{
	}
}