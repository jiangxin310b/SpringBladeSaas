﻿using SpringBlade.WeiXinCore.Model.Wx;
namespace SpringBlade.WeiXinService.Interfaces
{
    /*!
    * 文件名称：Wx_setting服务接口
    */
	public interface IWxSettingService: IBaseServer<WxSetting>
	{
	}
}